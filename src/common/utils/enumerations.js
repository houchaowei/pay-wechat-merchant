module.exports = {
  //会员卡状态
  verifyMobile: {
    yes: {
      key: '1',
      value: '验证手机号'
    },

    no: {
      key: '0',
      value: '不验证手机号'
    }
  },

  //授权状态
  isNeedGetOauth: {

    yes: {
      key: '3',
      value: '需要授权'
    },

    no: {
      key: '1',
      value: '不需要授权'
    }

  },

  //发卡方式
  cardIssue: {

    negative: {
      key: '-1',
      value: '手动领卡'
    },

    positive: {
      key: '0',
      value: '免费自动领卡'
    },

    rule: {
      key: '-2',
      value: '根据储值规则领卡'
    },

    pay: {
      key: '0',
      value: '大于0表示支付金额领卡'
    }


  }

}
