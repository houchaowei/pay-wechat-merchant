module.exports = {
  /**
   * 获取url参数
   * @returns {Object}
   */
  getUrlParam (name) {
    // let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    // let r = window.location.search.substr(1).match(reg);
    // if (r !== null) {
    //   return decodeURIComponent(decodeURIComponent(r[2]));
    // }
    // return null;
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    let search = window.location.href.replace('#/', '').split('?')

    if(this.isEmptyObject(search) || search.length <= 1) {
      return null
    }

    search = search[1]

    let r = search.match(reg);

    if (r !== null) {
      return decodeURIComponent(decodeURIComponent(r[2]));
    }
    return null;
  },


  /**
   * 判断空字符串
   * @param str
   * @returns {boolean}
   */
  isEmptyString(str) {
    return str === '' || str === undefined ||
      str === null || str === 'null';
  },

  /**
   * 判断空对象
   * @param obj
   * @returns {boolean}
   */
  isEmptyObject(obj) {
    for (var key in obj) {
      return false;
    }

    return true;
  },

  /**
   * 验证手机号
   * @param obj
   * @returns {boolean}
   */
  checkMobile(mobile) {
    const reg = /^1[3|4|5|7|8][0-9]{9}$/;
    if(!reg.test(mobile)){
      return false;
    }

    return true;
  }
}
