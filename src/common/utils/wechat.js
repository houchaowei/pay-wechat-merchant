const common = require('./common')
const enumerations = require('./enumerations')
const error = require('./error')

export const wechat = {

  callPay: function (webPayData, _this) {
    if (typeof WeixinJSBridge == "undefined") {
      if (document.addEventListener) {
        // alert("必须在微信中支付");
        document.addEventListener('WeixinJSBridgeReady', this.onBridgeReady, false);
      } else if (document.attachEvent) {
        // alert("必须在微信中支付2");
        document.attachEvent('WeixinJSBridgeReady', this.onBridgeReady);
        document.attachEvent('onWeixinJSBridgeReady', this.onBridgeReady);
      }
    } else {
      this.onBridgeReady(webPayData, _this);
    }
  },

  onBridgeReady: function (webPayData, _this) {
    const config = {
      "appId": webPayData.appId,     //公众号名称，由商户传入
      "timeStamp": webPayData.timeStamp,         //时间戳，自1970年以来的秒数
      "nonceStr": webPayData.nonceStr, //随机串
      "package": webPayData.package,
      "signType": webPayData.signType,         //微信签名方式：
      "paySign": webPayData.paySign //微信签名
    };
    WeixinJSBridge.invoke(
      'getBrandWCPayRequest', config,
      function (res) {
        if (res.err_msg == "get_brand_wcpay_request:ok") {
          _this.$router.push({
            name: 'Status',
            params: {'cardNo': this.cardNo}
          })//zhif
        }     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。
      }
    );
  }


}
