// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import VueRouter from 'vue-router'
import axios from 'axios'
import qs from 'qs'
import apiRoute from './assets/js/api-route'
import 'lib-flexible'
import App from './App'
import Common from  './common/utils/common'

import Register from  './components/Register.vue' // 注册
import Modify from './components/Modify.vue' // 修改资料
import Center from './components/Center.vue' // 会员中心
import BindMember from './components/BindMember.vue' // 绑定会员
import My from './components/My.vue' //我的资料
import Tickets from './components/Tickets.vue' // 优惠券
import DealRecord from './components/DealRecord.vue' // 交易流水
import Error from './components/Error.vue' //404页面
import Balance from './components/Balance.vue' //我的储值
import Status from './components/Status.vue' //状态页
import BuyCard from './components/BuyCard.vue' //状态页
import MemberCard from './components/MemberCard.vue' //  会员卡

// import DealInfo from './components/DealInfo.vue' // 交易详情
// import Pay from './components/Pay.vue' //支付页面
// import Login from './components/Login.vue' //404页面

Vue.prototype.$http = axios
Vue.prototype.$qs = qs
Vue.prototype.$apiRoute = apiRoute
Vue.Common = Common
Vue.use(VueRouter)

const routes = [
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/modify',
    name: 'Modify',
    component: Modify
  },
  {
    path: '/center',
    name: 'Center',
    component: Center
  },
  {
    path: '/bind',
    name: 'bindMember',
    component: BindMember
  },
  {
    path: '/my',
    name: 'My',
    component: My
  },
  {
    path: '/tickets',
    name: 'Tickets',
    component: Tickets
  },
  {
    path: '/dealRecord',
    name: 'DealRecord',
    component: DealRecord
  },
  {
    path: '/error',
    name: 'Error',
    component: Error
  },
  {
    path: '/balance/:cardNo.html',
    name: 'Balance',
    component: Balance
  },
  {
    path: '/status',
    name: 'Status',
    component: Status
  },
  {
    path: '/buy',
    name: 'BuyCard',
    component: BuyCard
  },
  {
    path: '/memberCard/:cardNo',
    name: 'MemberCard',
    component: MemberCard
  },
  // {
  //   path: '/discountCard',
  //   name: 'discountCard',
  //   component: DiscountCard
  // },
  // {
  //   path: '/dealInfo/:batchNo/:traceNo',
  //   name: 'DealInfo',
  //   component: DealInfo
  // },
  // {
  //   path: '/pay',
  //   name: 'Pay',
  //   component: Pay
  // },
  // {
  //   path: '/login',
  //   name: 'Login',
  //   component: Login
  // }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

FastClick.attach(document.body)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App)
}).$mount('#app-box')
