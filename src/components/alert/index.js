import MyAlert from './alert.vue'

const Alert = {
  install: function(Vue){
    Vue.component('Alert',MyAlert)
  }
}

export default Alert
