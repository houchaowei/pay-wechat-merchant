const head = 'http://tinterface.huitouke.cn/mb'
const oauthHead = 'http://tinterface.huitouke.cn/wechat'

// const head = 'http://192.168.31.136:8080/mb'
// const oauthHead = 'http://192.168.31.136:8080/wechat'//'http://192.168.31.136:8080/wechat'
const payHead = 'http://111.1.41.104:9090'

export default {
  //payHead
  getPay: payHead + '/posservice',

  //oauthHead
  oauth: oauthHead + '/oauth',
  getMchType: oauthHead + '/getMchType',
  payOauth: oauthHead + '/payOauth',//支付授权

  //head
  checkNeedMobileCode: head + '/checkNeedMobileCode',
  sendMobileCode: head + '/sendMobileCode',
  register: head + '/doRegister',
  bindMbSendMobileCode: head + '/bindMbSendMobileCode',
  bindMb: head + '/bindMb',
  checkMbInfo: head + '/checkMbInfo',
  center: head + '/center',
  cardInfo: head + '/cardInfo',
  mbInfo: head + '/mbInfo',
  tradeList: head + '/tradeList',
  couponList: head + '/couponList',
  myQrcode: head + '/myQrcode',
  getBalanceInfo: head + '/rechargeIndex',
  createRechargeDiscountOrder: head + '/createRechargeDiscountOrder',//选择充值金额
  createRechargeOrder: head + '/createRechargeOrder',//输入充值金额

  // wechatFollowInfo: head + '/wechatFollowInfo',
  // info: head + '/mbInfo',
  // doModify: head + '/updateMbInfo',
  // tradeInfo: head + '/tradeInfo',
  // cardList: head + '/cardList',
  // addWechatMember: head + '/addWechatMember',
  addWechatMemberCard: head + '/addWechatMemberCard',
}
